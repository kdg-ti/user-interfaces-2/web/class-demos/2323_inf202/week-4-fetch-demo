# De demo's zijn te vinden op andere locaties:

### Fetch demo:

[Fetch Demo #1](https://gitlab.com/kdg-ti/user-interfaces-2/web/fetch_demo)


### Extra uitbredingen op de fetch demo:
(meerdere promises genest in elkaar)  
[Fetch Demo #2](https://gitlab.com/kdg-ti/user-interfaces-2/web/fetch_demo/-/tree/nested-fetch?ref_type=heads)